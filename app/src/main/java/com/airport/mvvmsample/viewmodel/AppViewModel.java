package com.airport.mvvmsample.viewmodel;

import com.airport.mvvmsample.models.BookItemListModel;


import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.airport.mvvmsample.models.NewsItemListModel;
import com.airport.mvvmsample.repository.AppRepository;
import com.airport.mvvmsample.web.Resource;


import java.util.HashMap;

import javax.inject.Inject;



public class AppViewModel extends ViewModel {
    private AppRepository appRepository;

    @Inject
    public AppViewModel(AppRepository appRepository) {
        this.appRepository = appRepository;
    }


    public LiveData<Resource<NewsItemListModel>> getNewsList(String url) {
        return appRepository.getNewsList(url);
    }

   public LiveData<Resource<BookItemListModel>> getBookList(String url) {
        return appRepository.getBookList(url);
    }


}
