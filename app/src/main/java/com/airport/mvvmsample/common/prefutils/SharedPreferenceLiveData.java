package com.airport.mvvmsample.common.prefutils;


import android.content.SharedPreferences;

import androidx.lifecycle.LiveData;



public abstract class SharedPreferenceLiveData<T> extends LiveData<T> {


    private final SharedPreferences sharedPrefs;
    private final String key;
    private final T defValue;

    private SharedPreferences.OnSharedPreferenceChangeListener preferenceChangeListener;

    public SharedPreferenceLiveData(SharedPreferences sharedPrefs, String key, T defValue) {
        this.sharedPrefs = sharedPrefs;
        this.key = key;
        this.defValue = defValue;

        preferenceChangeListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                if (SharedPreferenceLiveData.this.key == key) {
                    SharedPreferenceLiveData.this.setValue(SharedPreferenceLiveData.this.getValueFromPreferences(key, SharedPreferenceLiveData.this.getDefValue()));
                }
            }
        };

        if (SharedPreferenceLiveData.this.key == key) {
            SharedPreferenceLiveData.this.setValue(SharedPreferenceLiveData.this.getValueFromPreferences(key, SharedPreferenceLiveData.this.getDefValue()));
        }


    }

    @Override
    protected void onActive() {
        super.onActive();
        sharedPrefs.registerOnSharedPreferenceChangeListener(preferenceChangeListener);
    }

    @Override
    protected void onInactive() {
        super.onInactive();
        sharedPrefs.unregisterOnSharedPreferenceChangeListener(preferenceChangeListener);
    }

    public String getKey() {
        return key;
    }

    public T getDefValue() {
        return defValue;
    }

    public SharedPreferences getSharedPrefs() {
        return sharedPrefs;
    }

    public abstract T getValueFromPreferences(String key, T defValue);
}


