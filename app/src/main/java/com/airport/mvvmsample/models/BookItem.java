package com.airport.mvvmsample.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class BookItem implements Serializable {

    @SerializedName("kind")
    String kind;
    @SerializedName("id")
    String id;
    @SerializedName("etag")
    String etag;
    @SerializedName("selfLink")
    String selfLink;

    @SerializedName("volumeInfo")
    VolumeInfo volumeInfo;

    @SerializedName("saleInfo")
    SaleInfo saleInfo;

    @SerializedName("accessInfo")
    AccessInfo accessInfo;

    @SerializedName("searchInfo")
    SearchInfo searchInfo;


    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEtag() {
        return etag;
    }

    public void setEtag(String etag) {
        this.etag = etag;
    }

    public String getSelfLink() {
        return selfLink;
    }

    public void setSelfLink(String selfLink) {
        this.selfLink = selfLink;
    }

    public VolumeInfo getVolumeInfo() {
        return volumeInfo;
    }

    public void setVolumeInfo(VolumeInfo volumeInfo) {
        this.volumeInfo = volumeInfo;
    }

    public SaleInfo getSaleInfo() {
        return saleInfo;
    }

    public void setSaleInfo(SaleInfo saleInfo) {
        this.saleInfo = saleInfo;
    }

    public AccessInfo getAccessInfo() {
        return accessInfo;
    }

    public void setAccessInfo(AccessInfo accessInfo) {
        this.accessInfo = accessInfo;
    }

    public SearchInfo getSearchInfo() {
        return searchInfo;
    }

    public void setSearchInfo(SearchInfo searchInfo) {
        this.searchInfo = searchInfo;
    }

    public class VolumeInfo implements Serializable {


        @SerializedName("title")
        String title;
        @SerializedName("authors")
        List<String> authors;
        @SerializedName("publisher")
        String publisher;
        @SerializedName("publishedDate")
        String publishedDate;
        @SerializedName("description")
        String description;

        @SerializedName("readingModes")
        ReadingModes readingModes;

        @SerializedName("industryIdentifiers")
        List<IndusryIdenfier> indusryIdenfierList;


        @SerializedName("pageCount")
        int pageCount;
        @SerializedName("printType")
        String printType;
        @SerializedName("categories")
        List<String> categories;
        @SerializedName("maturityRating")
        String maturityRating;

        @SerializedName("allowAnonLogging")
        String allowAnonLogging;


        @SerializedName("contentVersion")
        String contentVersion;
        @SerializedName("panelizationSummary")
        PanelizationSummary panelizationSummary;

        @SerializedName("imageLinks")
        ImageLinks imageLinks;

        @SerializedName("language")
        String language;
        @SerializedName("previewLink")
        String previewLink;
        @SerializedName("infoLink")
        String infoLink;
        @SerializedName("canonicalVolumeLink")
        String canonicalVolumeLink;


        String authorstr;

        public String getAuthorstr() {
            return authorstr;
        }

        public void setAuthorstr(String authorstr) {

            this.authorstr = authorstr;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public List<String> getAuthors() {
            return authors;
        }

        public void setAuthors(List<String> authors) {
            this.authors = authors;

            StringBuilder authorstr1= new StringBuilder();
            for ( int i=0 ;i< authors.size() ;i++) {
                authorstr1.append(authors.get(i)).append(",\n");
            }
            setAuthorstr(authorstr1.toString());
        }

        public String getPublisher() {
            return publisher;
        }

        public void setPublisher(String publisher) {
            this.publisher = publisher;
        }

        public String getPublishedDate() {
            return publishedDate;
        }

        public void setPublishedDate(String publishedDate) {
            this.publishedDate = publishedDate;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public ReadingModes getReadingModes() {
            return readingModes;
        }

        public void setReadingModes(ReadingModes readingModes) {
            this.readingModes = readingModes;
        }

        public List<IndusryIdenfier> getIndusryIdenfierList() {
            return indusryIdenfierList;
        }

        public void setIndusryIdenfierList(List<IndusryIdenfier> indusryIdenfierList) {
            this.indusryIdenfierList = indusryIdenfierList;
        }

        public int getPageCount() {
            return pageCount;
        }

        public void setPageCount(int pageCount) {
            this.pageCount = pageCount;
        }

        public String getPrintType() {
            return printType;
        }

        public void setPrintType(String printType) {
            this.printType = printType;
        }

        public List<String> getCategories() {
            return categories;
        }

        public void setCategories(List<String> categories) {
            this.categories = categories;
        }

        public String getMaturityRating() {
            return maturityRating;
        }

        public void setMaturityRating(String maturityRating) {
            this.maturityRating = maturityRating;
        }

        public String getAllowAnonLogging() {
            return allowAnonLogging;
        }

        public void setAllowAnonLogging(String allowAnonLogging) {
            this.allowAnonLogging = allowAnonLogging;
        }

        public String getContentVersion() {
            return contentVersion;
        }

        public void setContentVersion(String contentVersion) {
            this.contentVersion = contentVersion;
        }

        public PanelizationSummary getPanelizationSummary() {
            return panelizationSummary;
        }

        public void setPanelizationSummary(PanelizationSummary panelizationSummary) {
            this.panelizationSummary = panelizationSummary;
        }

        public ImageLinks getImageLinks() {
            return imageLinks;
        }

        public void setImageLinks(ImageLinks imageLinks) {
            this.imageLinks = imageLinks;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getPreviewLink() {
            return previewLink;
        }

        public void setPreviewLink(String previewLink) {
            this.previewLink = previewLink;
        }

        public String getInfoLink() {
            return infoLink;
        }

        public void setInfoLink(String infoLink) {
            this.infoLink = infoLink;
        }

        public String getCanonicalVolumeLink() {
            return canonicalVolumeLink;
        }

        public void setCanonicalVolumeLink(String canonicalVolumeLink) {
            this.canonicalVolumeLink = canonicalVolumeLink;
        }
    }

    public class SaleInfo implements Serializable {

        @SerializedName("country")
        String country;

        @SerializedName("saleability")
        String saleability;
        @SerializedName("isEbook")
        String isEbook;
        @SerializedName("listPrice")
        ListPrice listPrice;
        @SerializedName("amount")
        String amount;
        @SerializedName("currencyCode")
        String currencyCode;
        @SerializedName("buyLink")
        String buyLink;
        @SerializedName("retailPrice")
        RetailPrice retailPrice;

        @SerializedName("offers")
        List<Offers> offers;


        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getSaleability() {
            return saleability;
        }

        public void setSaleability(String saleability) {
            this.saleability = saleability;
        }

        public String getIsEbook() {
            return isEbook;
        }

        public void setIsEbook(String isEbook) {
            this.isEbook = isEbook;
        }

        public ListPrice getListPrice() {
            return listPrice;
        }

        public void setListPrice(ListPrice listPrice) {
            this.listPrice = listPrice;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getCurrencyCode() {
            return currencyCode;
        }

        public void setCurrencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
        }

        public String getBuyLink() {
            return buyLink;
        }

        public void setBuyLink(String buyLink) {
            this.buyLink = buyLink;
        }

        public RetailPrice getRetailPrice() {
            return retailPrice;
        }

        public void setRetailPrice(RetailPrice retailPrice) {
            this.retailPrice = retailPrice;
        }

        public List<Offers> getOffers() {
            return offers;
        }

        public void setOffers(List<Offers> offers) {
            this.offers = offers;
        }
    }

    public class AccessInfo implements Serializable {

        @SerializedName("country")
        String country;

        @SerializedName("viewability")
        String viewability;

        @SerializedName("embeddable")
        boolean embeddable;

        @SerializedName("publicDomain")
        boolean publicDomain;

        @SerializedName("textToSpeechPermission")
        String textToSpeechPermission;

        @SerializedName("isAvailable")
        boolean isAvailable;

        @SerializedName("pdf")
        PDF pdf;

        @SerializedName("epub")
        PDF epub;

        @SerializedName("acsTokenLink")
        String acsTokenLink;

        @SerializedName("webReaderLink")
        String webReaderLink;

        @SerializedName("accessViewStatus")
        String accessViewStatus;

        @SerializedName("quoteSharingAllowed")
        boolean quoteSharingAllowed;


        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getViewability() {
            return viewability;
        }

        public void setViewability(String viewability) {
            this.viewability = viewability;
        }

        public boolean isEmbeddable() {
            return embeddable;
        }

        public void setEmbeddable(boolean embeddable) {
            this.embeddable = embeddable;
        }

        public boolean isPublicDomain() {
            return publicDomain;
        }

        public void setPublicDomain(boolean publicDomain) {
            this.publicDomain = publicDomain;
        }

        public String getTextToSpeechPermission() {
            return textToSpeechPermission;
        }

        public void setTextToSpeechPermission(String textToSpeechPermission) {
            this.textToSpeechPermission = textToSpeechPermission;
        }

        public boolean isAvailable() {
            return isAvailable;
        }

        public void setAvailable(boolean available) {
            isAvailable = available;
        }

        public PDF getPdf() {
            return pdf;
        }

        public void setPdf(PDF pdf) {
            this.pdf = pdf;
        }

        public PDF getEpub() {
            return epub;
        }

        public void setEpub(PDF epub) {
            this.epub = epub;
        }

        public String getAcsTokenLink() {
            return acsTokenLink;
        }

        public void setAcsTokenLink(String acsTokenLink) {
            this.acsTokenLink = acsTokenLink;
        }

        public String getWebReaderLink() {
            return webReaderLink;
        }

        public void setWebReaderLink(String webReaderLink) {
            this.webReaderLink = webReaderLink;
        }

        public String getAccessViewStatus() {
            return accessViewStatus;
        }

        public void setAccessViewStatus(String accessViewStatus) {
            this.accessViewStatus = accessViewStatus;
        }

        public boolean isQuoteSharingAllowed() {
            return quoteSharingAllowed;
        }

        public void setQuoteSharingAllowed(boolean quoteSharingAllowed) {
            this.quoteSharingAllowed = quoteSharingAllowed;
        }
    }

    public class SearchInfo implements Serializable {

        @SerializedName("textSnippet")
        String textSnippet;

        public String getTextSnippet() {
            return textSnippet;
        }

        public void setTextSnippet(String textSnippet) {
            this.textSnippet = textSnippet;
        }
    }

    public class PanelizationSummary implements Serializable {

        @SerializedName("containsEpubBubbles")
        String containsEpubBubbles;

        @SerializedName("containsImageBubbles")
        String containsImageBubbles;


        public String getContainsEpubBubbles() {
            return containsEpubBubbles;
        }

        public void setContainsEpubBubbles(String containsEpubBubbles) {
            this.containsEpubBubbles = containsEpubBubbles;
        }

        public String getContainsImageBubbles() {
            return containsImageBubbles;
        }

        public void setContainsImageBubbles(String containsImageBubbles) {
            this.containsImageBubbles = containsImageBubbles;
        }
    }

    public class ImageLinks implements Serializable {

        @SerializedName("smallThumbnail")
        String smallThumbnail;

        @SerializedName("thumbnail")
        String thumbnail;

        public String getSmallThumbnail() {
            return smallThumbnail;
        }

        public void setSmallThumbnail(String smallThumbnail) {
            this.smallThumbnail = smallThumbnail;
        }

        public String getThumbnail() {
            return thumbnail;
        }

        public void setThumbnail(String thumbnail) {
            this.thumbnail = thumbnail;
        }
    }

    public class ListPrice implements Serializable {


        @SerializedName("amount")
        String amount;

        @SerializedName("currencyCode")
        String currencyCode;


        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getCurrencyCode() {
            return currencyCode;
        }

        public void setCurrencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
        }
    }

    public class RetailPrice implements Serializable {


        @SerializedName("amount")
        String amount;

        @SerializedName("currencyCode")
        String currencyCode;

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getCurrencyCode() {
            return currencyCode;
        }

        public void setCurrencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
        }
    }


    public class ReadingModes implements Serializable {

        @SerializedName("text")
        boolean text;
        @SerializedName("image")
        boolean image;

        public boolean isText() {
            return text;
        }

        public void setText(boolean text) {
            this.text = text;
        }

        public boolean isImage() {
            return image;
        }

        public void setImage(boolean image) {
            this.image = image;
        }
    }


    public class ListRetails implements Serializable {

        @SerializedName("amountInMicros")
        String amountInMicros;

        @SerializedName("currencyCode")
        String currencyCode;

        public String getAmountInMicros() {
            return amountInMicros;
        }

        public void setAmountInMicros(String amountInMicros) {
            this.amountInMicros = amountInMicros;
        }

        public String getCurrencyCode() {
            return currencyCode;
        }

        public void setCurrencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
        }
    }


    public class Offers implements Serializable {


        @SerializedName("finskyOfferType")
        String finskyOfferType;


        @SerializedName("listPrice")
        ListRetails listPrice;


        @SerializedName("retailPrice")
        ListRetails retailPrice;


        public String getFinskyOfferType() {
            return finskyOfferType;
        }

        public void setFinskyOfferType(String finskyOfferType) {
            this.finskyOfferType = finskyOfferType;
        }

        public ListRetails getListPrice() {
            return listPrice;
        }

        public void setListPrice(ListRetails listPrice) {
            this.listPrice = listPrice;
        }

        public ListRetails getRetailPrice() {
            return retailPrice;
        }

        public void setRetailPrice(ListRetails retailPrice) {
            this.retailPrice = retailPrice;
        }
    }

    public class PDF implements Serializable {

        @SerializedName("isAvailable")
        boolean isAvailable;


        @SerializedName("acsTokenLink")
        String acsTokenLink;

        public boolean isAvailable() {
            return isAvailable;
        }

        public void setAvailable(boolean available) {
            isAvailable = available;
        }

        public String getAcsTokenLink() {
            return acsTokenLink;
        }

        public void setAcsTokenLink(String acsTokenLink) {
            this.acsTokenLink = acsTokenLink;
        }
    }

    public class IndusryIdenfier implements Serializable{

        @SerializedName("type")
        String type;


        @SerializedName("identifier")
        String identifier;


        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getIdentifier() {
            return identifier;
        }

        public void setIdentifier(String identifier) {
            this.identifier = identifier;
        }
    }


}
