package com.airport.mvvmsample.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.airport.mvvmsample.R;
import com.airport.mvvmsample.databinding.ActivityNewsDetailsBinding;

import dagger.android.AndroidInjection;

public class NewsDetailsActivity extends BaseActivity {

    ActivityNewsDetailsBinding binding;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);
        binding = DataBindingUtil.bind(getRootView());
        String url = getIntent().getStringExtra("url");
        String title = getIntent().getStringExtra("title");
        setTitle(title);
        setHomeEnabled(true);

        binding.webview.getSettings().setJavaScriptEnabled(true);
        binding.webview.loadUrl(url);



        binding.webview.setWebViewClient(new WebViewClient(){

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url){
                view.loadUrl(url);
                return true;
            }
        });

    }
}