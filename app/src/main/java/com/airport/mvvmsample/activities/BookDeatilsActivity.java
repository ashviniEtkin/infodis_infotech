package com.airport.mvvmsample.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.airport.mvvmsample.R;
import com.airport.mvvmsample.databinding.ActivityBookDeatilsBinding;
import com.airport.mvvmsample.models.BookItem;

import dagger.android.AndroidInjection;

public class BookDeatilsActivity extends BaseActivity {

    BookItem bookItem = new BookItem();
    ActivityBookDeatilsBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_book_deatils);
        binding = DataBindingUtil.bind(getRootView());






        setHomeEnabled(true);
        bookItem =(BookItem) getIntent().getSerializableExtra("bookdata");
        StringBuilder authorstr1= new StringBuilder();
        for ( int i=0 ;i< bookItem.getVolumeInfo().getAuthors().size() ;i++) {
            authorstr1.append(bookItem.getVolumeInfo().getAuthors().get(i)).append(",\n");
        }
        binding.auther.setText(authorstr1);
        setTitle(bookItem.getVolumeInfo().getTitle());
        binding.setBookdata(bookItem);
        binding.setThumbnail(bookItem.getVolumeInfo().getImageLinks().getThumbnail());
        binding.setVolumndata(bookItem.getVolumeInfo());
        binding.moreinfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(bookItem.getVolumeInfo().getInfoLink().startsWith("http://books.google.co.in/books")){
                    Intent newsintent = new Intent(BookDeatilsActivity.this, NewsDetailsActivity.class);
                    newsintent.putExtra("url", bookItem.getVolumeInfo().getInfoLink());
                    newsintent.putExtra("title", bookItem.getVolumeInfo().getTitle());
                    startActivity(newsintent);
                }else{

                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(bookItem.getVolumeInfo().getInfoLink())));

                }
            }
        });
    }
}